package com.ranull.instantnetherportals.manager;

import com.ranull.instantnetherportals.InstantNetherPortals;
import org.bukkit.entity.Player;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class InvulnerableManager {
    private final InstantNetherPortals plugin;
    private boolean playerAbilitiesOldType;
    private String playerAbilitiesName = null;
    private String invulnerableName = null;

    public InvulnerableManager(InstantNetherPortals plugin) {
        this.plugin = plugin;
    }

    public void setTemporaryInvulnerable(Player player) {
        setInvulnerable(player, true);
        plugin.getServer().getScheduler().runTaskLater(plugin, () -> setInvulnerable(player, false), 2L);
    }

    public void setInvulnerable(Player player, boolean bool) {
        if (playerAbilitiesName == null) {
            setPlayerAbilitiesMethodName(player);
        }

        if (playerAbilitiesName != null && invulnerableName == null) {
            setInvulnerableBooleanName(player);
        }

        if (playerAbilitiesName != null && invulnerableName != null) {
            try {
                Object playerAbilitiesObject = getPlayerAbilitiesObject(player);

                if (playerAbilitiesObject != null) {
                    Field invulnerableField = playerAbilitiesObject.getClass().getDeclaredField(invulnerableName);

                    invulnerableField.setAccessible(true);
                    invulnerableField.setBoolean(playerAbilitiesObject, bool);
                    invulnerableField.setAccessible(false);
                }
            } catch (IllegalAccessException | NoSuchFieldException exception) {
                plugin.getLogger().info("Error: " + exception.getMessage());
            }
        } else {
            plugin.getLogger().info("Error: Deobfuscation failed");
        }
    }

    private Object getPlayerAbilitiesObject(Player player) {
        try {
            Object playerObject = player.getClass().getMethod("getHandle").invoke(player);

            return playerAbilitiesOldType ? playerObject.getClass().getSuperclass()
                    .getDeclaredField(playerAbilitiesName).get(playerObject) : playerObject.getClass().getSuperclass()
                    .getMethod(playerAbilitiesName).invoke(playerObject);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException
                 | NoSuchFieldException exception) {
            plugin.getLogger().info("Error: " + exception.getMessage());
        }

        return null;
    }

    private void setPlayerAbilitiesMethodName(Player player) {
        try {
            Object playerObject = player.getClass().getMethod("getHandle").invoke(player);

            for (Method method : playerObject.getClass().getSuperclass().getMethods()) {
                if (method.getReturnType().getName().endsWith("PlayerAbilities")) {
                    playerAbilitiesOldType = false;
                    playerAbilitiesName = method.getName();

                    return;
                }
            }

            for (Field field : playerObject.getClass().getSuperclass().getDeclaredFields()) {
                if (field.getType().getName().endsWith("PlayerAbilities")) {
                    playerAbilitiesOldType = true;
                    playerAbilitiesName = field.getName();

                    return;
                }
            }
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException exception) {
            plugin.getLogger().info("Error: " + exception.getMessage());
        }
    }

    private void setInvulnerableBooleanName(Player player) {
        Object playerAbilitiesObject = getPlayerAbilitiesObject(player);

        if (playerAbilitiesObject != null) {
            invulnerableName = playerAbilitiesObject.getClass().getDeclaredFields()[0].getName();
        }
    }
}
