package com.ranull.instantnetherportals;

import com.ranull.instantnetherportals.listener.PlayerMoveListener;
import com.ranull.instantnetherportals.manager.InvulnerableManager;
import org.bstats.bukkit.MetricsLite;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;

public class InstantNetherPortals extends JavaPlugin {
    private InvulnerableManager invulnerableManager;

    @Override
    public void onEnable() {
        invulnerableManager = new InvulnerableManager(this);

        registerMetrics();
        registerListeners();
    }

    @Override
    public void onDisable() {
        unregisterListeners();
    }

    private void registerMetrics() {
        new MetricsLite(this, 12871);
    }

    public void registerListeners() {
        getServer().getPluginManager().registerEvents(new PlayerMoveListener(this), this);
    }

    public void unregisterListeners() {
        HandlerList.unregisterAll(this);
    }

    public InvulnerableManager getInvulnerableManager() {
        return invulnerableManager;
    }
}
