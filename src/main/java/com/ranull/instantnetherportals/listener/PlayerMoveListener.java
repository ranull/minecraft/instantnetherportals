package com.ranull.instantnetherportals.listener;

import com.ranull.instantnetherportals.InstantNetherPortals;
import org.bukkit.GameMode;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class PlayerMoveListener implements Listener {
    private final InstantNetherPortals plugin;

    public PlayerMoveListener(InstantNetherPortals plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerMove(PlayerMoveEvent event) {
        Player player = event.getPlayer();

        if (!(player.getGameMode() == GameMode.CREATIVE) && player.hasPermission("instantnetherportals.use")
                && event.getTo() != null && (!isNetherPortal(event.getFrom().getBlock())
                && isNetherPortal(event.getTo().getBlock()))) {
            plugin.getInvulnerableManager().setTemporaryInvulnerable(player);
        }
    }

    private boolean isNetherPortal(Block block) {
        return block.getType().name().equals("NETHER_PORTAL") || block.getType().name().equals("PORTAL");
    }
}
